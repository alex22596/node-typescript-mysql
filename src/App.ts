import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as helmet from "helmet";
import * as morgan from "morgan";
import * as errorHandling from "./apiV1/users/errorHandler.middleware";
import apiV1 from "./apiV1/users/user.route";

class App {
  public express: express.Application;

  constructor() {
    this.express = express();
    this.setMiddlewares();
    this.setRoutes();
    this.catchErrors();
  }

  private setMiddlewares(): void {
    this.express.use(cors());
    this.express.use(morgan("dev"));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(helmet());
  }

  private setRoutes(): void {
    this.express.use("/", apiV1);
  }

  private catchErrors(): void {
    this.express.use(errorHandling.catch404);
    this.express.use(errorHandling.errorHandler);
  }
}

export default new App().express;
