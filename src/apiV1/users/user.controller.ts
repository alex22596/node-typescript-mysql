import { Request, Response } from 'express';
import MySQL from '../../config/db';

export default class UserController {
  public findAll = (req: Request, res: Response): void => {
    const query = `
      SELECT *
      FROM Users
    `;
    MySQL.execQuery(query)
      .then(users => {
        res.status(200).send({
          ok: true,
          users
        });
      })
      .catch(err => {
        res.status(400).send({
          ok: false,
          error: err
        });
      });
  };
}
