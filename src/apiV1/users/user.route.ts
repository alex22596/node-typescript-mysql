import { Router } from "express";
import Controller from './user.controller';

const user: Router = Router();
const controller = new Controller();

// Retrieve all Users
user.get('/', controller.findAll);

// // Create a New User
// user.post('/', controller.create);

// // Update a User with Username
// user.put('/:username', controller.update);

// // Delete a User with Username
// user.delete('/:username', controller.remove);

export default user;
