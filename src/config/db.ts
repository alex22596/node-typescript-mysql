import * as mysql from 'mysql2';

class MySQL {
  public static get instance() {
    return this._instance || (this._instance = new this());
  }

  public static execQuery(query: string) {
    return new Promise((resolve, reject) => {
      this.instance.connection.query(query, (err, results: object[]) => {
        if (err) {
          console.log('Query Error!');
          console.log(err);
          reject(err);
        }
        if (results.length === 0) {
          resolve('There are no Inserted Values');
        } else {
          resolve(results);
        }
      });
    });
  }
  // tslint:disable-next-line:variable-name
  private static _instance: MySQL;
  private connection: mysql.Connection;

  constructor() {
    this.connection = mysql.createConnection({
      host: '0.0.0.0',
      user: 'root',
      password: 'admin',
      port: 3306,
      database: 'genfores_db'
    });
    this.connectDB();
  }

  private connectDB() {
    this.connection.connect((err: mysql.MysqlError) => {
      if (err) {
        console.log(err.message);
        return;
      }
      console.log('Database is On!');
    });
  }
}

export default MySQL;
