import app from './App';
import CONFIG from './config/config';
import './config/db';

const PORT = 3000;

app.listen(PORT, err => {
  if (err) {
    console.log(err);
  }
  console.log(`Server is listening on ${PORT}`);
});
